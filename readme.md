# [RADIOGAGA] | [BENJAMIN FELTIN]

## Prérequis logiciel :

- MySQL
- Node
- Angular

___

## Instructions pour lancer l'application :


  - Importer la base de données (fichier radiogaga.sql) dans le moteur MySQL
  - Lancer le moteur MySQL
  - Modifier le fichier `config.js` présent dans le dossier JS du projet. Il contient la configuration pour se connecter à la base de données
  - Se placer dans le dossier JS et lancer la commande suivante pour lancer le backend :
```
node app.js
```
  - Le serveur se lance sur le port 3000
  - Se rendre dans le dossier Front/radio-gaga et lancer la commande
```
ng serve
```
  - l'application est accessible à cette adresse <localhost:4200>
