// ici, on se connecte à la base de données. Ainsi, on pourra utiliser
// db (l'équivalent de PDO) pour réaliser les requêtes mySQL.
const config = require("./config");
const db = require("./mysqlConnect");

// chaque requête correspond à une fonction qui renverra ce que l'on appelle
// une Promise (promesse). Une promesse est un objet qui contient une
// fonction (dont on sait qu'elle sera exécutée dans le futur). La promesse
// est renvoyée avant que la fonction ne soit exécutée (fonctionnement donc
// asynchrone). Quand la fonction a été exécutée, la callback appelle la
// fonction resolve qui indique à la promesse qu'elle peut renvoyer la
// réponse en question. Dans le fichier getCours1.js, les lignes 40 et 41
// (celles avec les await) récupèrent ces Promises. L'opérateur await attend
// alors que la promesse soit résolue (resolve) et récupère alors la
// réponse. Ainsi, même si tout ce fonctionnement est asynchrone, la variable
// idsPetitsCours de la ligne 40 du fichier getCours1.js récupérera le
// résultat de la requête mysql quand celui-ci aura été renvoyé par le
// serveur MySQL.

function getRadios() {
    let query = `
        SELECT * FROM ${config.mysqlRadios}`;
    return new Promise((resolve, reject) => {
        db.query(query, (err, rows) => {
            if (err) {
                process.stdout.write("❌❌❌\n");
                return reject(err);
            }
            process.stdout.write("✔✔✔\n");
            resolve(rows);
        });
    });
}
module.exports.getRadios = getRadios;




function insertRadios(radio, livestream) {
    let query = `
        INSERT INTO ${config.mysqlRadios} (ID, nom, flux)
        VALUES ('', '${radio}', '${livestream}')`;

    return new Promise((resolve, reject) => {
        db.query(query, (err, rows) => {
            if (err) {
                process.stdout.write("❌❌❌\n");
                return reject(err);
            }
            process.stdout.write("✔✔✔\n");
            resolve(rows);
        });
    });
}
module.exports.insertRadios = insertRadios;