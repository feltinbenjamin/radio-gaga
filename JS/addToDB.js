const fs = require('fs');
const filename = './res.json';
const file = JSON.parse(fs.readFileSync(filename));

const queries = require('./mysqlQueries');


const radios = [];
const frequences = [];


file['brands'].forEach((radio) => {
    // console.log(e.id)
    radios.push(radio.id);
    frequences.push(radio.liveStream);

    if (radio.localRadios != null) {
        radio.localRadios.forEach((local) => {
            radios.push(local.id);
            frequences.push(local.liveStream);
        })
    }

    if (radio.webRadios != null) {
        radio.webRadios.forEach((web) => {
            radios.push(web.id);
            frequences.push(web.liveStream);
        })
    }


})


for (let i = 0; i < radios.length; i++) {
    queries.insertRadios(radios[i], frequences[i])

}


console.log(radios);
console.log(frequences);